package com.example.ro36;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

// makes possible to schedule methods to run at certain times
@EnableScheduling
@SpringBootApplication
public class Ro36Application {

	public static void main(String[] args) {
		SpringApplication.run(Ro36Application.class, args);
	}

}
