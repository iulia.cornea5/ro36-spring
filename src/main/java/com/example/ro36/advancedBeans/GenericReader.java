package com.example.ro36.advancedBeans;

public interface GenericReader {

    String readLine();
}
