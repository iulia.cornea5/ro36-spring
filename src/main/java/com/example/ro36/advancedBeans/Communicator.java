package com.example.ro36.advancedBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class Communicator {

//    @Autowired
//    ReaderOfFile rf;
//
//    @Autowired
//    ReaderOfKeyboard rb;

    @Autowired
//    @Qualifier("mySpecialNameForReaderOfFile")
    GenericReader genericReader;

    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void readStuff() {
//        System.out.println("Read from file: " + rf.readLine());
//        System.out.println("Read from keyboard: " + rb.readLine());
//        System.out.println("Read from ?: " + genericReader.readLine());
        if(genericReader.getClass().equals(ReaderOfKeyboard2.class) ) {
            System.out.println("ReaderOfKeyboard2");
        }
    }
}
