package com.example.ro36.advancedBeans;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Primary
@Component
public class ReaderOfKeyboard2 implements GenericReader{

    Scanner keyboard;
    ReaderOfKeyboard2() {
        this.keyboard = new Scanner(System.in);
    }

    @Override
    public String readLine() {
        return keyboard.nextLine();
    }
}
