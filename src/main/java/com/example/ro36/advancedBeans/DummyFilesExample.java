package com.example.ro36.advancedBeans;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

@Configuration
public class DummyFilesExample {

//    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void fileReadWriteExample() {

        try {
            FileReader reader = new FileReader("C:\\Users\\iulia\\Downloads\\ro36\\ro36\\src\\main\\resources\\read.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String firstLine = bufferedReader.readLine();
            System.out.println(firstLine);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
