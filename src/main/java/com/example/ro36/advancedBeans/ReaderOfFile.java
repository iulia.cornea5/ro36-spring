package com.example.ro36.advancedBeans;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


//ReaderOfFile mySpecialNameForReaderOfFile = new ReaderOfFile()

@Component("mySpecialNameForReaderOfFile")
public class ReaderOfFile implements  GenericReader{

    BufferedReader bufferedReader;

    public ReaderOfFile() throws FileNotFoundException {
        this.bufferedReader = new BufferedReader(new FileReader("C:\\Users\\iulia\\Downloads\\ro36\\ro36\\src\\main\\resources\\read.txt"));
    }

    @Override
    public String readLine() {
        try {
            return this.bufferedReader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
