package com.example.ro36.advancedBeans;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ReaderOfKeyboard implements GenericReader{

    Scanner keyboard;
    ReaderOfKeyboard () {
        this.keyboard = new Scanner(System.in);
    }

    @Override
    public String readLine() {
        return keyboard.nextLine();
    }
}
