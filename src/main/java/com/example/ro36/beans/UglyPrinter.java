package com.example.ro36.beans;

import org.springframework.stereotype.Component;

@Component
public class UglyPrinter {

    public void uglyPrint(String message) {
        System.out.println("from ugly printer: " + message);
    }
}
