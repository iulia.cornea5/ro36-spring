package com.example.ro36.beans;

import org.springframework.stereotype.Component;

@Component
public class PrettyPrinter {

    public PrettyPrinter() {}

    public void prettyPrint(String messsage) {
        System.out.println("-------------------");
        System.out.println("| " + messsage + " |");
        System.out.println("-------------------");

    }
}
