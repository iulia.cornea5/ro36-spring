package com.example.ro36.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
public class Scheduler {

    // Bean
    @Autowired
    PrettyPrinter pp;

    private NotSoPrettyPrinter nspp;

    public Scheduler(NotSoPrettyPrinter nspp) {
        this.nspp = nspp;
    }

    private UglyPrinter up;

    @Autowired
    public void setUp(UglyPrinter up) {
        this.up = up;
    }

    // fixed delay is in seconds
    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void every10Minutes() {
        pp.prettyPrint("Am ajuns aici");
        nspp.noSoPrettyPrin("ceva ceva");
        up.uglyPrint("some ugly message");
    }
}
