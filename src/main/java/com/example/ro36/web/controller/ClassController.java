package com.example.ro36.web.controller;

import com.example.ro36.jpa.entity.ClassEntity;
import com.example.ro36.jpa.entity.Student;
import com.example.ro36.jpa.repository.ClassEntityRepository;
import com.example.ro36.jpa.repository.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/class")
public class ClassController {

    private final ClassEntityRepository classRepository;
    private final StudentRepository studentRepository;

    public ClassController(ClassEntityRepository classRepository, StudentRepository studentRepository) {
        this.classRepository = classRepository;
        this.studentRepository = studentRepository;
    }

    // returnează toti studenții dintr-o clasă
    // http://localhost:8080/api/class/12_C/students <- http://localhost:8080 (din spring) + /api/class (de pe controller/clasă) + /12_C/students (de pe metodă)
    // în URL-uri caracterul " " (space) e înlocuit cu "%20"
    @GetMapping("/{class_name}/students")
    public ResponseEntity getAllStudentsFromClass(@PathVariable("class_name") String className) {

        // căutăm classEntity după NUME (12_C) ca să îi alfăm id-ul (id-ul 2)
        ClassEntity classEntity = classRepository.findByName(className);
        if(classEntity == null) {
            return new ResponseEntity<String>("No such class could be found", HttpStatus.BAD_REQUEST);
        }

        Integer classId = classEntity.getId();
        List<Student> students = studentRepository.findByClassId(classId);
        return ResponseEntity.ok(students);
    }

}
