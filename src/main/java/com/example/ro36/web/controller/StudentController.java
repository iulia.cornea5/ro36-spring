package com.example.ro36.web.controller;

import com.example.ro36.jpa.entity.Student;
import com.example.ro36.jpa.repository.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/api/students")
public class StudentController {

    private final StudentRepository repository;

    // added to constructor so Spring injects it
    public StudentController(StudentRepository repository) {
        this.repository = repository;
    }

    //    @RequestMapping(
//            method = RequestMethod.GET,
//            // http://localhost:8080/api/students
//            path = "/api/students"
//    )
    @GetMapping()
    public ResponseEntity<List<Student>> getAllStudents(@RequestParam(name = "first_name", required = false) String firstName) {
        List<Student> students;
        if (firstName == null) {
            students = repository.findAll();
        } else {
            students = repository.findByFirstName(firstName);
        }

//        ResponseEntity<List<Student>> response = new ResponseEntity<>(students, HttpStatus.OK);

        return ResponseEntity.ok(students);
    }

//    @RequestMapping(
//            method = RequestMethod.GET,
//            // http://localhost:8080/api/students
//            path = "/api/students"
//    )
//    @GetMapping("api/students")
//    public ResponseEntity<List<Student>> getAllStudentsByFirstName(@RequestParam(name = "first_name") String firstName) {
//
//        List<Student> students = repository.findByFirstName(firstName);
//        return ResponseEntity.ok(students);
//    }


}
