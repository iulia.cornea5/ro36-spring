package com.example.ro36.ex1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
public class Communicator {

    @Autowired
    Greeter greeter;
    @Autowired
    Reader reader;

    @Scheduled(fixedDelay = 1000 * 60)
    public void runIntroductions() {
        greeter.presentYourselfAndAskForIntroduction();
        String person = reader.getName();
        greeter.greet(person);
    }
}
