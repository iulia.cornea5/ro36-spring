package com.example.ro36.ex1;

import org.springframework.stereotype.Component;

@Component
public class Greeter {

    public void presentYourselfAndAskForIntroduction() {
        System.out.println("I'm Dory! Who are you?");
    }

    public void greet(String person) {
        System.out.println("Hi, " + person + "!" );
    }
}