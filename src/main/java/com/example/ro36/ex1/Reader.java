package com.example.ro36.ex1;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Reader {

    Scanner keyboard = new Scanner(System.in);

    public String getName() {
        return keyboard.nextLine();
    }
}
