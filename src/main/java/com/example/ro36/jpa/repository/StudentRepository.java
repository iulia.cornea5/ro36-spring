package com.example.ro36.jpa.repository;

import com.example.ro36.jpa.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

// annotation for spring to create a bean of SimpleJPARepository<Student, Integer>
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    // JpaRepository <- holds functionality for data management

    List<Student> findByFirstName(String fn);

    List<Student> findByFirstNameAndLastName(String fn, String ln);

    List<Student> findByClassId(Integer classId);

}
