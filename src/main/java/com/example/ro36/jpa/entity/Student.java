package com.example.ro36.jpa.entity;

import jdk.jfr.DataAmount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.Date;

@Data
@ToString
@Entity(name = "students")
@Deprecated(since = "from 2022 7 aug please use StudentEntity instead")
public class Student {

    @Id
    private int id;

    private String firstName;
    private String lastName;

    @Column(name = "birth_date")
    private LocalDate dateOfBirth;

    private Integer classId;

}
