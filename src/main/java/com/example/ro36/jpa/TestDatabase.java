package com.example.ro36.jpa;

import com.example.ro36.jpa.entity.ClassEntity;
import com.example.ro36.jpa.entity.Student;
import com.example.ro36.jpa.repository.ClassEntityRepository;
import com.example.ro36.jpa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Scanner;

@Configuration
public class TestDatabase {

    // toti profesorii care predau matematica
    //      Entity for Teacher
    //      Repository for Teacher
    //      in Repository for Teacher findBySubject findByMaterie
    // toti studentii ditnr-o clasa
    // cati studenti sunt in clasa x
    // gasit studenti dupa data nasterii
    // gasit studentii care sunt nascuti intre data1 si data2
    // gasit cel mai tanar student
    // cel mai bine platit profesor
    // media salariala a profesorilor


    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ClassEntityRepository classEntityRepository;

    @Scheduled(fixedDelay = 1000 * 60)
    public void testDatabase() {
        findStudentsFromAClass();
        queryFindAll();
        queryByFirstName("george");
        queryByFirstNameAndLastName("ana", "blandiana");

    }

    public void queryFindAll() {
        List<Student> studentList = studentRepository.findAll();
        studentList.forEach(student ->
                System.out.println(
                        student.getFirstName() + " " + student.getLastName()
                )
        );
    }

    public void queryByFirstName(String firstName) {
        System.out.println(studentRepository.findByFirstName(firstName));
    }

    public void queryByFirstNameAndLastName(String firstName, String lastName) {
        System.out.println(studentRepository.findByFirstNameAndLastName(firstName, lastName));
    }

    public void findStudentsFromAClass() {
        System.out.println("Dati clasa de interes: ");
        Scanner keyboard = new Scanner(System.in);
        String className = keyboard.nextLine();     // 11_A

        ClassEntity studentClass = classEntityRepository.findByName(className);
        List<Student> students = studentRepository.findByClassId(studentClass.getId());
        students.forEach(System.out::println);
    }
}
